<?php

use Illuminate\Support\Facades\Route;

Route::delete('', 'AccessTokenController@destroy')->name('api.v1.token.destroy');
