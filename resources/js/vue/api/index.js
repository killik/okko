import axios from 'axios'
import endpoint from "./endpoint"

// Auth
const token = JSON.parse(localStorage.getItem('token') || null)

// Axios instance config
const config = {
    baseURL: process.env.API_BASEURL,
    headers: {
        'Authorization': `Bearer ${token && token.plain}`
    }
}

const http = axios.create(config)

const fetch = (name, data = null, params = []) => {
    const method = endpoint[name][0]
    let route = endpoint[name][1]

    if (params.length) {
        const find = route.match(/\{(.*?)\}/g)
        for (let i = 0; i < params.length; i++) {
            route = route.replace(find[i], params[i])
        }
    }

    return http[method](route, data)
}

export default {
    fetch
}
