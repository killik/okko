// Routes

import tesComponent from "../../components/pages/admin/tes"

const routes = [
    {
        path: "",
        component: tesComponent,
        name: "admin",
    },
]

export default routes
