<?php

namespace App;

use App\Traits\Model\Relations\HasMany\HasManyComments;
use App\Traits\Model\Relations\HasOne\HasOneOwner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Post extends Model
{
    use HasManyComments;
    use HasOneOwner;

    public function thumbnail(): HasOne
    {
        return $this->cover(Picture::class);
    }
}
