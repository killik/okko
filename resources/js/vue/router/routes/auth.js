// Routes

import loginComponent    from "../../components/pages/auth/login"
import registerComponent from "../../components/pages/auth/register"

const routes = [
    {
        path: "",
        name: "auth",
        beforeEnter: (_to, _from, next) => next({ name: "auth.login" })
    },

    {
        path: "login",
        component: loginComponent,
        name: "auth.login",
    },

    {
        path: "logout",
        name: "auth.logout",
        beforeEnter (_to, _from, next) {
            next(vm => vm.$store.dispatch('auth/logout'))
        }
    },

    {
        path: "register",
        component: registerComponent,
        name: "auth.register",
    }
]

export default routes
