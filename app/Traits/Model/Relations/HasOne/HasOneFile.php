<?php

namespace App\Traits\Model\Relations\HasMany;

use App\File;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait HasOneFile {
    public function file(): HasOne
    {
        return $this->hasMany(File::class);
    }
}
