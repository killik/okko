<?php

namespace App;

use App\Traits\Model\Relations\HasOne\HasOneOwner;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasOneOwner;
}
