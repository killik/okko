import store from "../../store"

// Guest Guard

export default async (to, _from, next) => {

    if (store.getters['auth/isLoggedIn']) {
        return next('admin')
    }

    next()
}
