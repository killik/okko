<?php

namespace App;

use App\Traits\Model\Relations\HasOne\HasOneOwner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Comment extends Model
{
    use HasOneOwner;

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }
}
