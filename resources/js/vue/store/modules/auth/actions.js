import API from "../../../api"

export default {
    login: ({commit}, {email,password}) => {
        return API
            .fetch('authLogin', { email, password, device: navigator.userAgent })
            .then(() => {
                localStorage.setItem('token', JSON.stringify(res.data.data.token))
                commit('SET_LOGGED')
                commit('SET_VERIFIED')
            })
            .catch(() => {
                commit('UNSET_LOGGED')
                commit('UNSET_VERIFIED')
            })
    },

    verify: ({commit}, {token}) => {
        return API
            .fetch('authVerify', { token })
            .then((res) => {
                commit('SET_VERIFIED')
                commit('SET_USER', { user: res.data.data })
            })
            .catch(() => commit('UNSET_VERIFIED'))
    },

    destroy: ({}, {id}) => {
        return API
            .fetch('authDestroy', { id })
    },

    logout ({commit,dispatch}) {
        const {id} = JSON.parse(localStorage.getItem('token') || null) || 0

        return dispatch('destroy', {id})
            .then(() => {
                commit('UNSET_LOGGED')
                commit('UNSET_VERIFIED')
                commit('UNSET_USER')

                localStorage.removeItem('token')
            })
    }
}
