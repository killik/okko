// API Route / Endpoint list
export default {
    authLogin: ['post', '/api/v1/auth/login'],
    authVerify: ['post', '/api/v1/auth/verify'],






















    auth_session: ['get', '/auth/token'],
    auth_login_operator: ['post', '/auth/login/operator'],
    auth_destroy: ['delete', '/auth/destroy'],

    operator_index: ['get', '/operator'],
    operator_store: ['post', '/operator'],
    operator_update: ['patch', '/operator/{id}'],
    operator_destroy: ['delete', '/operator'],
    operator_find: ['get', '/operator/find/{name|username|email}']
}
