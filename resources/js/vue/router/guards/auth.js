import store from "../../store"

// Auth Guard

export default async (to, _from, next) => {

    if (store.getters['auth/isLoggedIn']) {
        if (!store.getters['auth/isVerified']) {
            const { plain } = JSON.parse(localStorage.getItem('token') || null) || 0
            store
                .dispatch('auth/verify', {token: plain})
                .then(() => next())
                .catch(err => {
                    if (err.response) {
                        localStorage.removeItem('token')
                        next('auth')
                    }
                })
        }
    }

    else next('auth')
}
