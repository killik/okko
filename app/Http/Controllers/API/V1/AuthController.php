<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\Auth\LoginRequest;
use App\Http\Requests\API\V1\Auth\VerifyRequest;
use App\Http\Resources\API\V1\AccessTokenResource;
use App\Http\Resources\API\V1\UserResource;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\PersonalAccessToken;

class AuthController extends Controller
{
    public function login (LoginRequest $request)
    {
        $user = User::whereEmail($request->email)->first(['password', 'id']);

        if($user && Hash::check($request->password, $user->password))
        {
            return new AccessTokenResource($user->createToken($request->device));
        }

        throw ValidationException::withMessages(['email' => [trans('auth.failed')]]);
    }

    public function verify (VerifyRequest $request) {
        if($token = PersonalAccessToken::findToken($request->token)) {
            return new UserResource($token->tokenable);
        }

        throw ValidationException::withMessages(['token' => [trans('auth.failed')]]);
    }
}
