<?php

namespace App\Traits\Model\Relations\HasMany;

use App\Comment;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasManyComments {
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }
}
