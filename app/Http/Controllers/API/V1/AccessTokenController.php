<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\AccessToken\DestroyRequest;
use Illuminate\Http\Request;

class AccessTokenController extends Controller
{
    public function destroy (DestroyRequest $request) {
        $request
            ->user()
            ->tokens()
            ->find($request->id)
            ->delete();
    }
}
