<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class AccessTokenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "token" => [
                "id"            => $this->accessToken->id,
                "name"          => $this->accessToken->name,
                "abilities"     => $this->accessToken->abilities,
                "plain"         => $this->plainTextToken,
                "updated_at"    => $this->accessToken->updated_at,
                "created_at"    => $this->accessToken->created_at,
            ],

            "user" => new UserResource($this->accessToken->tokenable)
        ];
    }
}
