import actions from "./actions"

export default {
    namespaced: true, actions,

    state: {
        isLoggedIn: !!localStorage.getItem('token'),
        isVerified: false,
        user: null
    },

    getters: {
        isLoggedIn: state => state.isLoggedIn,
        isVerified: state => state.isVerified,
        user: state => state.user
    },

    mutations: {
        SET_LOGGED: (state) => state.isLoggedIn = true,
        UNSET_LOGGED: (state) => state.isLoggedIn = false,
        SET_VERIFIED: (state) => state.isVerified = true,
        UNSET_VERIFIED: (state) => state.isVerified = false,
        SET_USER: (state, {user}) => state.user = user,
        UNSET_USER: (state) => state.user = null
    }
}
