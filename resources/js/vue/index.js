// VueJS Instance

import vue from "vue"

import router from "./router"
import store  from "./store"

const app = new vue({ router, store, render: creteElement => creteElement('router-view') })

app.$mount('#app')
