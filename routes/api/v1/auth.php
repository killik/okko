<?php

use Illuminate\Support\Facades\Route;

Route::post('login', 'AuthController@login')->name('api.v1.auth.login');
Route::post('verify', 'AuthController@verify')->name('api.v1.auth.verify');
