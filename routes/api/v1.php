<?php

use Illuminate\Support\Facades\Route;

Route::namespace('V1')->group(function () {

    Route::prefix('auth')->group(base_path('routes/api/v1/auth.php'));

    Route::middleware('auth:sanctum')->group(function () {

        Route::prefix('token')->group(base_path('routes/api/v1/token.php'));

    });
});
