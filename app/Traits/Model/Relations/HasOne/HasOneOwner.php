<?php

namespace App\Traits\Model\Relations\HasOne;

use App\User;
use Illuminate\Database\Eloquent\Relations\HasOne;

trait HasOneOwner {
    public function owner(): HasOne
    {
        return $this->hasOne(User::class);
    }
}
