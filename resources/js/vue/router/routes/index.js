// Routes
import adminComponent   from "../../components/pages/admin"
import adminRoutes      from "./admin"

import authComponent    from "../../components/pages/auth"
import authRoutes       from "./auth"

const routes = [
    {
        path: "/admin",
        component: adminComponent,
        children: adminRoutes
    },

    {
        path: "/auth",
        component: authComponent,
        children: authRoutes
    }
]

export default routes
